//this is my release_node function. It basically frees the memory allocated to whatever node is put into the argument.
//the function has no return value and will be used in conjunction with the delete function.
//no side effects, which is the opposite of every medicine you see on tv.

#include "gcode.h"

void release_node(struct node *list){
   free(list);
}

