//Name: Tim Tipton
//Class: CSE 222
//Date: 1/25/2021
//Assignment: 2
//Synopsis: This is a program used to manipulate a linked list.
//It will create a linked list and let the user add numbers to it, delete numbers from it,
//print the list, search for numbers in the list, and it will give the user feedback if a 
//certain command can't be used or was unsuccessful.
#include<stdio.h>
#include<stdlib.h>
#include "gcode.h" //header file with everything I need for this program to run.
int number;//global variable to keep things consistent in my functions
void main(){

//creates a pointer to a struct node (list) this is my sentinel node
     struct node *list=init();//initializes an empty list.

   while(1){
   printf(">");//prints an arrow for a prompt

   char buffer[100];//creates a buffer to put info in for fgets.
   fgets(buffer, 100, stdin);
   char command;//variable to store the command (i.e.i, p, s, d, x)
   int items_read=sscanf(buffer,  "%c %d", &command, &number);//used to parse the input from fgets

   if(items_read==1 && command=='p'){//checks to see if p was the only input and then runs the print function if it was
      print(list);
   }

   else if(items_read==1 && command=='x'){//checks to see if x was the only input and then exits the program if it was after releasing all the nodes
      struct node *current=list->next;
      struct node *previous=list;
      while(current!=NULL){
         free(previous);
         previous=current;
         current=current->next;
      }
      free(previous);
      exit(0);
   }

   else if(items_read==2 && command=='i'){//checks to see if i and a number was input and if true searches for the number to add and adds it if the number isn't there
      int inList=search(list, number);
      if(inList==1){
         printf("NODE ALREADY IN LIST\n");//if number is in list then tells the user
      } 
      else{
         int success=add(list, number);//if number isn't in the list tries to add it
         if(success=1){
            printf("SUCCESS\n");//if there's room, adds the number to the list and lets the user know that it was successful
         }
         else{
            printf("OUT OF MEMORY\n");//if there's no room informs user
         }  
      }
}
   else if(items_read==2 && command=='s'){//code to search for a number and let the user know if it's in the list
      int found= search(list, number);
      if(found==1){
         printf("FOUND\n");
      }
      else{
         printf("NOT FOUND\n");
      }
   }
   else if(items_read==2 && command=='d'){//code to delete an item from the list
      int inList=search(list, number);
      if(inList==1){//if item is in the list, deletes it and prints success
         delete(list, number);
         printf("SUCCESS\n");
      }
      else{
         printf("NODE NOT FOUND\n");//if item is not in the list prints this message
      }
   }
   else{//if the user inputs commands that are not recognized by the program thist message is printed (a list of valid commands)
      printf("Please enter one of the following commands:\n");
      printf("i number     insert number into the list\n");
      printf("p            print the list in order\n");
      printf("s number     search for number in the list\n");
      printf("d number     delete number from the list\n");
      printf("x            exit the program\n");
   }
}

}
