//this is my delete function. This function deletes a number from the list.
// there is no return value and no side effects.

#include "gcode.h"

void delete(struct node *list, int number){//takes a pointer to a struct node and an integer. The pointer is the sentinel node and the integer is the number being deleted.
   struct node *current=list->next;//sets the current variable to be the index of the next node in the list
   struct node *previous=list;//sets previous to the address of the sentinel node.

   while(current!=NULL){
      if(current->data==number){//if we found the number to delete
         previous->next=current->next;//deletes that node from the list
         release_node(current);//frees the memory of the node we just deleted
         return;
      }
      else{
         previous=current;//these two lines travers the list
         current=current->next;
      }
   }
}
