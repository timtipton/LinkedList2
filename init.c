//this is my init function. It will initialize LL to be an emply linked list.
//this function takesa one argument and has no return value.
//there are no side effects
#include <stdio.h>
#include "gcode.h"

struct node *init(){
   struct node *list;
   list=malloc(sizeof(struct node));//allocates space for a struct node and makes list a pointer to that struct node.
   list->next=NULL;//creates the sentinel node
return list;
}
