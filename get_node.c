//this is the get_node function.
//this function checks the return value of malloc to see if there's room for another node in the linked list and returns the address of the new node or Null if you're out of space.

#include "gcode.h"

struct node *get_node(struct node *list){
   struct node *valid=malloc(sizeof(struct node));
   if(valid!=NULL)
      return (valid);
   else
      return NULL;
   
}
