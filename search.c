//``this is my search function.
//it searches my linked list for a node containing a certain number.
//it returns 1 if the node is found and 0 if not.
//this will be useful for things like the add function.
//no side effects.
#include<stdio.h>
#include "gcode.h"

int search(struct node *list, int number){
   struct node *current=list->next;
   while(current!=NULL){
      if(current->data==number){ 
        return 1;
      }
      else{
       current=current->next;
      }
   }
   return 0;
} 

