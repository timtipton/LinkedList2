//this is my include file. It has my function prototypes and my struct node set up
//I called it gcode.h so that way I could include a little G code into each of my files :-)

#include <stdlib.h>

struct node{
   int data;
   struct node *next;
};

struct node *init();

int add(struct node *list, int number);

void print(struct node *list);

void delete(struct node *list, int number);

int search(struct node *list, int number);

struct node *get_node(struct node *list);

void release_node(struct node *list);
