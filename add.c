//this is my add function
//this adds a number to the data field of the linked list
//the number will be added in order from smallest to biggest so
//that my linked list will be sorted.
//the function returns 1 if successful and 0 if not and I guess you could call
//having a new node in the linked list a side effect if you want.
//#include<stdlib.h>
#include<stdio.h>
#include "gcode.h"

int add(struct node *list, int number){//takes a pointer to a struct node and a number for arguments... the pointer is where the address of the sentinel node is and the number will be the data field of that node.

   struct node *new=get_node(list);//sets a variable to keep track of the index of the new node.
   new->data=number;
   struct node *previous_node=list;//variable to keep track of the previous node. Needed to insert number before the current node.
   struct node *current_node=list->next;//keeps track of the current node as the list is being traversed

   while(current_node!=NULL){
      if(current_node->data>number){//looking for the first number bigger than "number"
         previous_node->next=new;//points the previous node to the new node
         new->next=current_node;//points the new node to the current node (basically sliding it into the middle like when your dog wants to cuddle with you and your significant other)
         return 1;
      }
      else{
         current_node=current_node->next;//these two lines travers the list if the current node isn't mynull
         previous_node=previous_node->next;
      }  
   }
   previous_node->next=new;//these next three lines add a number to the end of the list.   
   new->next=current_node;
   new->data=number;
   return 1;
 
//  if(LL[99].next==MYNULL)//returns 0 if the list is full.
  //   return 0;    
}

