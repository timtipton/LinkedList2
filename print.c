//this is going to be my print function.
//its job will be to print the nodes in the linked list
//the function is void so there is no return value.
//the function has only one argument and there are no side effects from this function.
#include<stdio.h>
#include "gcode.h"

void print(struct node *list){
   struct node *current=list->next; //sets the pointer current to whatever the sentinel node is pointing at.
   while(current != NULL){  //loop to print each data field in the linked list
      printf("%d ", current->data);
      current=current->next; //traverses the linked list
   }
   printf("\n");
}

